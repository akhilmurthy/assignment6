FROM ubuntu:14.04
RUN apt-get update -y \ 
    && apt-get install -y python-setuptools python-pip python-mysqldb
ADD requirements.txt /requirements.txt
RUN pip install -r requirements.txt
ADD . .
EXPOSE 5000
CMD ["python", "movies_web_app.py"] 