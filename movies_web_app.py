import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode

application = Flask(__name__, template_folder = "templates")
app = application

#akhilmurthy, ar601128
def get_db_creds_aws_hard_coded():
    db = 'assignment4'
    username = 'akhilmurthy'
    password = 'ar601128'
    hostname = 'akhil.c4cd0y4wdbk8.us-east-1.rds.amazonaws.com'
    return db, username, password, hostname

def get_db_creds_aws_dynamic():
    db = os.environ.get("RDS_DB_NAME", None)
    username = os.environ.get("RDS_USERNAME", None)
    password = os.environ.get("RDS_PASSWORD", None)
    hostname = os.environ.get("RDS_HOSTNAME", None)
    return db, username, password, hostname

def get_db_creds_gcp_hard_coded():
    db = 'testdb'
    username = 'root'
    password = 'testpass123!@#'
    hostname = '35.193.132.13'
    return db, username, password, hostname

def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname



def create_table():
    # Check if table exists or not. 
    db, username, password, hostname = get_db_creds()

    table_ddl = 'CREATE TABLE Movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year INT, title TEXT, director TEXT, actor TEXT, release_date TEXT, rating DOUBLE, PRIMARY KEY (id))'
    print(table_ddl)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password = password, database=db, host = hostname)
        print("got here")
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table")
    create_table()
    print("After create_table")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


@app.route('/add_movie', methods=['POST'])
def add_movie():
    print("Received request.")
    year = request.form['year']
    title1 = request.form['title']
    director1 = request.form['director']
    actor1 = request.form['actor']
    release_date1 = request.form['release_date']
    rating = request.form['rating']

    title = title1.lower()
    director = director1.lower()
    actor = actor1.lower()
    release_date = release_date1.lower()


    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    select_command = "SELECT * FROM Movies WHERE title = '" + title  + "'"
    cur.execute(select_command)
    rows = cur.fetchall()
    if len(rows)!= 0:
        message = "Movie with title " + title + " already exists"
        return render_template('index.html', message=message)
    else:
        command = "INSERT INTO Movies (year, title, director, actor, release_date, rating) values (" + year +",'" + title + "', '" + director + "', '" + actor + "', '" + release_date + "', " + rating +")"
        try:
            cur.execute(command)
            cnx.commit()
            message = "Movie " + title + " successfully inserted."
            return render_template('index.html', message=message)
        except Exception as e:
            message = "Movie with title " + title + " could not be inserted - " + e
            return render_template('index.html', message=message)

@app.route('/update_movie', methods=['POST'])
def update_movie():
    print("Received request.")
    year = request.form['year']
    title1 = request.form['title']
    director1 = request.form['director']
    actor1 = request.form['actor']
    release_date1 = request.form['release_date']
    rating = request.form['rating']

    director = director1.lower()
    actor = actor1.lower()
    release_date = release_date1.lower()
    title = title1.lower()

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    select_command = "SELECT * FROM Movies WHERE title = '" + title  + "'"
    cur.execute(select_command)
    rows = cur.fetchall()
    if len(rows)== 0:
        message = "No Movie with title " + title + " exists in the database."
        return render_template('index.html', message=message)
    else:
        command = "UPDATE Movies SET year = " + year +" , director = '" + director+"', actor = '" + actor +"', release_date = '" + release_date +"' , rating = " + rating + " WHERE title = '" + title + "'"    
        try:
            cur.execute(command)
            cnx.commit()
            message = "Movie " + title + " successfully updated."
            return render_template('index.html', message=message)
        except Exception as e:
            message = "Movie with title " + title + " could not be updated - " + e
            return render_template('index.html', message=message)

@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    print("Received request.")
    title1 = request.form['delete_title']

    title = title1.lower()

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    select_command = "SELECT * FROM Movies WHERE title = '" + title  + "'"
    cur.execute(select_command)
    rows = cur.fetchall()
    if len(rows)== 0:
        message = "Movie with title " + title + " does not exist."
        return render_template('index.html', message=message)
    else:
        command = "DELETE FROM Movies WHERE title = '" + title +"'"
        try:
            cur.execute(command)
            cnx.commit()
            message = "Movie " + title + " successfully deleted."
            return render_template('index.html', message=message)
        except Exception as e:
            message = "Movie with title " + title + " could not be deleted - " + e
            return render_template('index.html', message=message)

@app.route('/search_movie', methods=['GET'])
def search_movie():
    print("Received request.")
    actor1 = request.args['search_actor']

    actor = actor1.lower()

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    select_command = "SELECT * FROM Movies WHERE actor = '" + actor  + "'"
    cur.execute(select_command)
    rows = cur.fetchall()
    if len(rows)== 0:
        message = "No Movies found for Actor " + actor 
        return render_template('index.html', message=message)
    else:
        command = "SELECT * FROM Movies WHERE actor = '" + actor +"'"
        cur.execute(command)
        rows = cur.fetchall()
        response = []

        for item in rows:
            message = "<" + item[2] + ", " + str(item[1]) + ", " + item[4] + ">" 
            response.append(message)

        return render_template('index.html', response=response)

@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    print("Received request.")

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    select_command = "SELECT * FROM Movies"
    cur.execute(select_command)
    rows = cur.fetchall()
    if len(rows)== 0:
        message = "No Movies found in the database."
        return render_template('index.html', message=message)
    else:
        command = "SELECT title, year, actor, director, rating FROM Movies WHERE rating = (SELECT MAX(rating) FROM Movies)"
        cur.execute(command)
        rows = cur.fetchall()
        response = []

        for item in rows:
            print(item)
            message = "<" + item[0] + ", " + str(item[1]) + ", " + item[2] + ", " + item[3] + ", " + str(item[4]) +  ">" 
            response.append(message)

        return render_template('index.html', response=response)

@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    print("Received request.")

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    select_command = "SELECT * FROM Movies"
    cur.execute(select_command)
    rows = cur.fetchall()
    if len(rows)== 0:
        message = "No Movies found in the database."
        return render_template('index.html', message=message)
    else:
        command = "SELECT title, year, actor, director, rating FROM Movies WHERE rating = (SELECT MIN(rating) FROM Movies)"
        cur.execute(command)
        rows = cur.fetchall()
        response = []

        for item in rows:
            print(item)
            message = "<" + item[0] + ", " + str(item[1]) + ", " + item[2] + ", " + item[3] + ", " + str(item[4]) +  ">" 
            response.append(message)

        return render_template('index.html', response=response)


@app.route("/")
def init():
    return render_template('index.html')




if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
